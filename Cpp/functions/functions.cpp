#include <algorithm> //std::sort
#include <unordered_set> //std::set
#include <vector>

//Adding hashing of vector<int> types
namespace std {
    template <>
    struct hash<std::vector<int>> {
        size_t operator()(const vector<int>& v) const {
					std::hash<int> hasher;
					size_t seed = 0;
					for (int i : v) {
						seed ^= hasher(i) + 0x9e3779b9 + (seed<<6) + (seed>>2);
					}
					return seed;
        }
    };
}

//order the patterns
std::vector<int> order(const std::vector<int> &Subperm, int k){

	std::vector<int> Subperm1(k);
	std::vector<int> OrderedSubperm = Subperm;

	std::sort(OrderedSubperm.begin(), OrderedSubperm.end());

	for (int x = 0; x < k; x++){
		Subperm1[ std::distance(Subperm.begin(),
		  std::find(Subperm.begin(),Subperm.end(),OrderedSubperm[x])) ] = x+1;
	}

	return Subperm1;
}

//turn an array in various patterns and insert pattern in
//setus set
std::unordered_set<std::vector<int>> paternize(std::vector<int> &perm, int k){

	int n = perm.size();

	std::unordered_set<std::vector<int>> setus;

	int counter = 0;
	std::vector<int> comb(k);
	std::vector<int> orderd_comb(k);
	std::vector<bool> v(n);
	std::fill(v.end() - k, v.end(), 1);

	do{
		counter = 0;
		for (int i = 0; i < n; ++i) {
			
			if (v[i]) {
				comb[counter] = perm[i];
				counter++;
			}
		}
		setus.insert(order(comb,k));
	} while( std::next_permutation(v.begin(), v.end()) );

	return setus;
}

//check if perm's set is a superpattern
bool superpattern_check(std::unordered_set<std::vector<int>> &setus, int k){
	bool bool0 = false;

	std::vector<int> kperm;
	for (int i=1; i<=k; i++){
		kperm.insert(kperm.end(),i);
	}

	do{
		bool0 = ( setus.find(kperm) == setus.end() );

		if ( bool0 ){
			break;
		}
	}while (std::next_permutation(kperm.begin(),kperm.end()));

	return !bool0;
}