#include "functions.cpp"

//order the patterns
std::vector<int> order(const std::vector<int> &Subperm, int k);

//turn an array in various patterns and insert pattern in
//setus set
std::unordered_set<std::vector<int>> paternize(std::vector<int> &perm, int k);

//check if perm's set is a superpattern
bool superpattern_check(std::unordered_set<int> &setus, std::vector<int> &kperm);
